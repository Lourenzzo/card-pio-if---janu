<?php

namespace app\controllers;

use Yii;
use app\models\JrNovo;
use app\models\JrNovoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * JrNovoController implements the CRUD actions for JrNovo model.
 */
class JrNovoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all JrNovo models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new JrNovoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single JrNovo model.
     * @param integer $cardapio
     * @param integer $prato
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($cardapio, $prato)
    {
        return $this->render('view', [
            'model' => $this->findModel($cardapio, $prato),
        ]);
    }

    /**
     * Creates a new JrNovo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new JrNovo();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'cardapio' => $model->cardapio, 'prato' => $model->prato]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing JrNovo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $cardapio
     * @param integer $prato
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($cardapio, $prato)
    {
        $model = $this->findModel($cardapio, $prato);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'cardapio' => $model->cardapio, 'prato' => $model->prato]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing JrNovo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $cardapio
     * @param integer $prato
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($cardapio, $prato)
    {
        $this->findModel($cardapio, $prato)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the JrNovo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $cardapio
     * @param integer $prato
     * @return JrNovo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($cardapio, $prato)
    {
        if (($model = JrNovo::findOne(['cardapio' => $cardapio, 'prato' => $prato])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
