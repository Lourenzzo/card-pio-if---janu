<?php
 
namespace app\controllers;
use Yii;
use yii\data\SqlDataProvider;

 
class RelatoriosController extends \yii\web\Controller
{
   public function actionIndex()
   {
       return $this->render('index');
   }

   public function actionRelatorio1()
   {
       $categoria = new SqlDataProvider([
        'sql' => 'SELECT jr_categoria.nome, COUNT(jr_categoria.id)AS QUANTIDADE
        FROM jr_pratos JOIN jr_categoria ON jr_pratos.categoria_id = jr_categoria.id
        GROUP BY jr_categoria.id
        ORDER BY COUNT(jr_categoria.ID) DESC',
            ]
        );
        
        return $this->render('relatorio1', ['resultado' => $categoria]);
   }


   public function actionRelatorio3()
   {
       $cardapio = new SqlDataProvider([
        'sql' => 'SELECT jr_cardapio.data, count(*) as Quantidade
        from jr_novo join jr_cardapio on jr_novo.cardapio = jr_cardapio.id
        group by jr_cardapio.id',
            ]
        );
        
        return $this->render('relatorio3', ['resultado' => $cardapio]);
   }

   public function actionRelatorio4()
   {
       $pratos = new SqlDataProvider([
        'sql' => 'SELECT jr_pratos.nome, count(*) as Quantidade
        from jr_pratos
        group by jr_pratos.nome',
            ]
        );
        
        return $this->render('relatorio4', ['resultado' => $pratos]);
   }

   public function actionRelatorio2()
   {
    $categoriap = new SqlDataProvider([
     'sql' => 'SELECT jr_categoria.nome, COUNT(*)AS QUANTIDADE
     FROM jr_categoria
     GROUP BY jr_categoria.nome',
        ]
     );
     
     return $this->render('relatorio2', ['resultado' => $categoriap]);
    }


   public function actionRelatorio5()
   {
       $categoria = new SqlDataProvider([
        'sql' => 'SELECT jr_cardapio.data, COUNT(jr_cardapio.id)AS QUANTIDADE
        FROM jr_cardapio 
        GROUP BY jr_cardapio.data
        ORDER BY COUNT(jr_cardapio.ID) DESC',
            ]
        );
        
        return $this->render('relatorio5', ['resultado' => $categoria]);
   }
}
?>