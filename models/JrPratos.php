<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jr_pratos".
 *
 * @property int $id
 * @property string $nome
 * @property int $categoria_id
 *
 * @property JrNovo[] $jrNovos
 * @property JrCardapio[] $cardapios
 * @property JrCategoria $categoria
 */
class JrPratos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jr_pratos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['categoria_id'], 'required'],
            [['categoria_id'], 'integer'],
            [['nome'], 'string', 'max' => 20],
            [['categoria_id'], 'exist', 'skipOnError' => true, 'targetClass' => JrCategoria::className(), 'targetAttribute' => ['categoria_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome' => 'Nome',
            'categoria_id' => 'Categoria',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJrNovos()
    {
        return $this->hasMany(JrNovo::className(), ['prato' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCardapios()
    {
        return $this->hasMany(JrCardapio::className(), ['id' => 'cardapio'])->viaTable('jr_novo', ['prato' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoria()
    {
        return $this->hasOne(JrCategoria::className(), ['id' => 'categoria_id']);
    }
}
