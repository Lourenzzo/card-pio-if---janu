<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jr_cardapio".
 *
 * @property int $id
 * @property string $data
 *
 * @property JrNovo[] $jrNovos
 * @property JrPratos[] $pratos
 */
class JrCardapio extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jr_cardapio';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id'], 'integer'],
            [['data'], 'safe'],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'data' => 'Data',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJrNovos()
    {
        return $this->hasMany(JrNovo::className(), ['cardapio' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPratos()
    {
        return $this->hasMany(JrPratos::className(), ['id' => 'prato'])->viaTable('jr_novo', ['cardapio' => 'id']);
    }
}
