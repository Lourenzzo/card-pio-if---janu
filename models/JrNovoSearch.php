<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\JrNovo;

/**
 * JrNovoSearch represents the model behind the search form of `app\models\JrNovo`.
 */
class JrNovoSearch extends JrNovo
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [[], 'integer'],
            [['prato','cardapio'], 'safe'],
        ];
    }
   

    public function attributes()
    {
    return array_merge(parent::attributes(), ['prato0.nome','cardapio0.data']);
   
    }
 

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = JrNovo::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $query->joinWith(['prato0']);
        $dataProvider->sort->attributes['prato0.nome']=[
            'asc'=>['nome'=>SORT_ASC],
            'desc'=>['nome'=>SORT_DESC],
        ];
        $query->joinWith(['cardapio0']);
        $dataProvider->sort->attributes['cardapio0.data']=[
            'asc'=>['data'=>SORT_ASC],
            'desc'=>['data'=>SORT_DESC],
        ];






        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'cardapio' => $this->cardapio,
            'prato' => $this->prato,
        ]);


        $query->andFilterWhere(['LIKE', 'prato0.nome',$this->getAttribute('prato0.nome')]);
        $query->andFilterWhere(['LIKE', 'cardapio0.data',$this->getAttribute('cardapio0.data')]);


        return $dataProvider;
    }
}
