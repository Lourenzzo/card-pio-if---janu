<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\JrPratos;

/**
 * JrPratosSearch represents the model behind the search form of `app\models\JrPratos`.
 */
class JrPratosSearch extends JrPratos
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
       
        return [
            [['id', ], 'integer'],
            [['nome','categoria.nome'], 'safe'],
        ];
    }

    public function attributes()
   {
   return array_merge(parent::attributes(), ['categoria.nome']);
   }



    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = JrPratos::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $query->joinWith(['categoria']);
        $dataProvider->sort->attributes['categoria.nome']=[
            'asc'=>['categoria.nome'=>SORT_ASC],
            'desc'=>['categoria.nome'=>SORT_DESC],
        ];
     

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'categoria_id' => $this->categoria_id,
        ]);

        $query->andFilterWhere(['LIKE', 'categoria.nome',$this->getAttribute('categoria.nome')]);

        
        return $dataProvider;
    }
}
