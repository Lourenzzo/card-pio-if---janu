<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jr_novo".
 *
 * @property int $cardapio
 * @property int $prato
 *
 * @property JrCardapio $cardapio0
 * @property JrPratos $prato0
 */
class JrNovo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jr_novo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cardapio', 'prato'], 'required'],
            [['cardapio', 'prato'], 'integer'],
            [['cardapio', 'prato'], 'unique', 'targetAttribute' => ['cardapio', 'prato']],
            [['cardapio'], 'exist', 'skipOnError' => true, 'targetClass' => JrCardapio::className(), 'targetAttribute' => ['cardapio' => 'id']],
            [['prato'], 'exist', 'skipOnError' => true, 'targetClass' => JrPratos::className(), 'targetAttribute' => ['prato' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cardapio' => 'Cardápio',
            'prato' => 'Prato',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCardapio0()
    {
        return $this->hasOne(JrCardapio::className(), ['id' => 'cardapio']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrato0()
    {
        return $this->hasOne(JrPratos::className(), ['id' => 'prato']);
    }
}
