<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jr_categoria".
 *
 * @property int $id
 * @property string $nome
 *
 * @property JrPratos[] $jrPratos
 */
class JrCategoria extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jr_categoria';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nome'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome' => 'Nome',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJrPratos()
    {
        return $this->hasMany(JrPratos::className(), ['categoria_id' => 'id']);
    }
}
