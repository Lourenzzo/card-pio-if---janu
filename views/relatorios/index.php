<?php
 
use yii\helpers\Html;
 
$this->title = 'Relatórios';
$this->params['breadcrumbs'][] = $this->title;
?>
 
<div class="relatorios-index">
 
   <h1><?= Html::encode($this->title) ?></h1>
   <?= Html::a('Quantidade de categorias', ['relatorio2'], ['class' => 'btn btn-success']) ?><br><br>
   
   <?= Html::a('Quantidade de cardápios por dia', ['relatorio5'], ['class' => 'btn btn-success']) ?>
   
   <br> <br>

   <?= Html::a('Quantidade de pratos por categoria', ['relatorio1'], ['class' => 'btn btn-success']) ?>
   <br> <br>
   <?= Html::a('Quantidade de pratos por cardápio', ['relatorio3'], ['class' => 'btn btn-success']) ?>   
   <br><br>
   <?= Html::a('Quantidade de pratos', ['relatorio4'], ['class' => 'btn btn-success']) ?>

</div>