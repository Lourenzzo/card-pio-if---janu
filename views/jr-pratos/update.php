<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\JrPratos */

$this->title = 'Update Jr Pratos: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Jr Pratos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="jr-pratos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
