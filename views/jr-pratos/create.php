<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\JrPratos */

$this->title = 'Criar pratos';
$this->params['breadcrumbs'][] = ['label' => 'Jr Pratos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jr-pratos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
