<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\JrCardapio */

$this->title = 'Criar Cardápio';
$this->params['breadcrumbs'][] = ['label' => 'Jr Cardapios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jr-cardapio-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
