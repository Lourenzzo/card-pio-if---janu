<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\JrCardapioSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cardápio';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jr-cardapio-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Criar cardápio', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute'=>'data',
                'filterInputOptions' => [
                    'type'=>'date',
                ],
            ],


            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
