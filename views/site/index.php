<?php

/* @var $this yii\web\View */

$this->title = 'Cardápio IF - Januária';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Seja bem-vindo!</h1>

        <p class="lead">Esse sistema tem o objetivo de montar/criar o cardápio do Instituto Federal – Campus Januária.</p>

        <p><a class="btn btn-lg btn-success" href="3">Vamos lá</a></p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>Problemática</h2>

                <p>Um dos problemas existentes no nosso campus é a falta de disponibilidade
                do cardápio as pessoas que frequentam o refeitório para fazer suas 
                refeições diárias, desde o café da manhã ao jantar. Observamos que muitas das vezes
                algumas pessoas não pode consumir o prato oferecido e só comem pelo fato de 
                ter comprado o ticket!   </p>

                <p><a class="btn btn-default" href="#"> + Sabor &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Solução</h2>

                <p> A solução que tivemos para suprir esse necessidade foi fazer um sistema
                no qual a nutricioista irá cadastrar os pratos que serão oferecidos no dia, 
                com isso as pessoas terão acesso a essas informações e poderá decidir se faz 
                sua refeição aqui no campus ou em casa! . </p>

                <p><a class="btn btn-default" href="#">+ Qualidade &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Quem somos!</h2>

                <p> Esse sistema foi planejado e feito pelos alunos: João Lourenço Jr. e Robert Lopes,
                do 2° Informática A . </p>

                <p><a class="btn btn-default" href="#">No seu dia a dia &raquo;</a></p>
            </div>
        </div>

    </div>
</div>
