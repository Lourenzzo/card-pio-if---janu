<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\JrCategoria */

$this->title = 'Criar categoria';
$this->params['breadcrumbs'][] = ['label' => 'Jr Categorias', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jr-categoria-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
