<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\jrCardapio;
use app\models\jrPratos;


/* @var $this yii\web\View */
/* @var $model app\models\JrNovo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="jr-novo-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'cardapio')-> 
     dropDownList(ArrayHelper::map(jrCardapio::find()
        ->orderBy('id')
        ->all(),'id','data'),
        ['prompt' => 'Selecione um cardápio'] )
    ?>



    <?= $form->field($model, 'prato')-> 
     dropDownList(ArrayHelper::map(jrPratos::find()
        ->orderBy('id')
        ->all(),'id','nome', 'categoria_id'),
        ['prompt' => 'Selecione um prato'] )
    ?>



    <div class="form-group">
        <?= Html::submitButton('Salvar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
