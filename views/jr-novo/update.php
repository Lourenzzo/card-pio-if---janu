<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\JrNovo */

$this->title = 'Update Jr Novo: ' . $model->cardapio;
$this->params['breadcrumbs'][] = ['label' => 'Jr Novos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cardapio, 'url' => ['view', 'cardapio' => $model->cardapio, 'prato' => $model->prato]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="jr-novo-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
