<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\JrNovo */

$this->title = $model->cardapio;
$this->params['breadcrumbs'][] = ['label' => 'Jr Novos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="jr-novo-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Atualizar', ['update', 'cardapio' => $model->cardapio, 'prato' => $model->prato], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Apagar', ['delete', 'cardapio' => $model->cardapio, 'prato' => $model->prato], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Tem certeza de que deseja excluir este item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'cardapio',
            'prato',
        ],
    ]) ?>

</div>
