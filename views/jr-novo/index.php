<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\JrNovoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Novo';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jr-novo-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Novo', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           
           
            ['attribute'=>'cardapio0.data','label'=>'Cardápio'],
            ['attribute'=>'prato0.nome','label'=>'Prato'],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
