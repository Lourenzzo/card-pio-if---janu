<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\JrNovo */

$this->title = 'Criar novo';
$this->params['breadcrumbs'][] = ['label' => 'Jr Novos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jr-novo-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
